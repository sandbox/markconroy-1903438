<?php
/**
 * @file
 * pinterest_gallery.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pinterest_gallery_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function pinterest_gallery_image_default_styles() {
  $styles = array();

  // Exported image style: pinterest_album_thumbnail.
  $styles['pinterest_album_thumbnail'] = array(
    'name' => 'pinterest_album_thumbnail',
    'effects' => array(
      3 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '230',
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: pinterest_gallery_fullsize.
  $styles['pinterest_gallery_fullsize'] = array(
    'name' => 'pinterest_gallery_fullsize',
    'effects' => array(
      2 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '1100',
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: pinterest_gallery_thumbnail.
  $styles['pinterest_gallery_thumbnail'] = array(
    'name' => 'pinterest_gallery_thumbnail',
    'effects' => array(
      1 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '195',
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function pinterest_gallery_node_info() {
  $items = array(
    'pinterest_gallery' => array(
      'name' => t('Pinterest Gallery'),
      'base' => 'node_content',
      'description' => t('Use this content type to create Pinterest-style galleries for your website. This module was created by <a href="https://drupal.org/user/336910">Mark Conroy</a> of <a href="http://www.adesignforlife.net">A Design For Life</a>.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
