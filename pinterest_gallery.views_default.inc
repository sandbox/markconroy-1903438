<?php
/**
 * @file
 * pinterest_gallery.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function pinterest_gallery_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'pinterest_galleries';
  $view->description = 'A view to show all the "Pinterest-style Galleries" on our website.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Pinterest Galleries';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Pinterest Galleries';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '40';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'masonry_views_grid';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['masonry_width'] = TRUE;
  $handler->display->display_options['style_options']['masonry_animated'] = TRUE;
  $handler->display->display_options['style_options']['masonry_animated_duration'] = TRUE;
  $handler->display->display_options['style_options']['masonry_resizable'] = TRUE;
  $handler->display->display_options['style_options']['masonry_center'] = TRUE;
  $handler->display->display_options['style_options']['masonry_gutter'] = FALSE;
  $handler->display->display_options['style_options']['masonry_rtl'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Add Gallery Images */
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['id'] = 'field_pinterest_gallery_images';
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['table'] = 'field_data_field_pinterest_gallery_images';
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['field'] = 'field_pinterest_gallery_images';
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['label'] = '';
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['settings'] = array(
    'image_style' => 'pinterest_album_thumbnail',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_pinterest_gallery_images']['delta_offset'] = '0';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div id="pinterest-gallery-wrapper">
<div id="pinterest-gallery-album-cover">[field_pinterest_gallery_images]</div>
<div id="pinterest-gallery-title">[title]</div>
</div>
';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'pinterest_gallery' => 'pinterest_gallery',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'pinterest-galleries';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Pinterest Galleries';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['pinterest_galleries'] = $view;

  return $export;
}
