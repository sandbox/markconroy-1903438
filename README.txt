Pinterest Gallery

Pinterest Gallery is an image gallery module built with the Features module to create an image gallery that resembles a <a href="http://pinterest.com/">Pinterest-style</a> board. 

It will create a content-type called "Pinterest Gallery", a view to list the Pinterest Galleries that you create, and also a menu item to link to this view. 

The images in the Pinterst Gallery will display in a Pinterst-style manner, and the view listing the galleries will also display in a Pinterest-style manner. 

By using the <a href="http://drupal.org/project/multiupload_imagefield_widget">Multi-upload Image Field</a> module, you can upload as many images in one go as you wish. If uploading a lot of images, be patient. 

This module was sponsored by <a href="http://www.adesignforlife.net">A Design For Life</a>. You can see a demonstration of it here. 

Non-Drupal Core Module Dependencies:

<a href="http://drupal.org/project/colorbox">Colorbox</a>
<a href="http://drupal.org/project/ctools">Chaos tools</a>
<a href="http://drupal.org/project/features">Features</a>
<a href="http://drupal.org/project/field_formatter_settings">Field formatter settings API</a>
<a href="http://drupal.org/project/libraries">Libraries</a>
<a href="http://drupal.org/project/masonry">Masonry</a>
Masonry Field Formatter - comes with Masonry module
Masonry Views - comes with Masonry module
<a href="http://drupal.org/project/views">Views</a>
<a href="http://drupal.org/project/multiupload_filefield_widget">Multiupload Filefield Widget</a>
<a href="http://drupal.org/project/multiupload_imagefield_widget">Multiupload Imagefield Widget</a>

Masonry jQuery Library Dependency

Download the jQuery Masonry plugin (jquery.masonry.min.js) to your libraries directory (should be available at /sites/.../libraries/masonry/jquery.masonry.min.js)
Download and enable the required modules listed above


