<?php
/**
 * @file
 * pinterest_gallery.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function pinterest_gallery_field_default_fields() {
  $fields = array();

  // Exported field: 'node-pinterest_gallery-field_pinterest_gallery_desc'.
  $fields['node-pinterest_gallery-field_pinterest_gallery_desc'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_pinterest_gallery_desc',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'pinterest_gallery',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Add a description for your gallery here if you wish.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'masonry' => 0,
            'masonry_animated' => 1,
            'masonry_animated_duration' => '500',
            'masonry_center' => 0,
            'masonry_gutter' => '0',
            'masonry_resizable' => 1,
            'masonry_rtl' => 0,
            'masonry_width' => '200',
          ),
          'type' => 'text_default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_pinterest_gallery_desc',
      'label' => 'Gallery Description',
      'required' => 0,
      'settings' => array(
        'display_summary' => 0,
        'text_processing' => '1',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '20',
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-pinterest_gallery-field_pinterest_gallery_images'.
  $fields['node-pinterest_gallery-field_pinterest_gallery_images'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_pinterest_gallery_images',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'pinterest_gallery',
      'deleted' => '0',
      'description' => 'Use this field to add your images to your gallery. <br>
You can add more than one image at a time, but be patient while they are uploading! <br>
Once uploaded, you can use the <em>Title</em> field for your caption on your image.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'colorbox',
          'settings' => array(
            'colorbox_caption' => 'title',
            'colorbox_caption_custom' => '',
            'colorbox_gallery' => 'post',
            'colorbox_gallery_custom' => '',
            'colorbox_image_style' => 'pinterest_gallery_fullsize',
            'colorbox_node_style' => 'pinterest_gallery_thumbnail',
            'masonry' => 1,
            'masonry_animated' => 1,
            'masonry_animated_duration' => '500',
            'masonry_center' => 1,
            'masonry_gutter' => '8',
            'masonry_resizable' => 1,
            'masonry_rtl' => 0,
            'masonry_width' => '220',
          ),
          'type' => 'colorbox',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_pinterest_gallery_images',
      'label' => 'Add Gallery Images',
      'required' => 0,
      'settings' => array(
        'alt_field' => 1,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '1500x1500',
        'min_resolution' => '',
        'title_field' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'multiupload_imagefield_widget',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_miw',
        'weight' => '3',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add Gallery Images');
  t('Add a description for your gallery here if you wish.');
  t('Gallery Description');
  t('Use this field to add your images to your gallery. <br>
You can add more than one image at a time, but be patient while they are uploading! <br>
Once uploaded, you can use the <em>Title</em> field for your caption on your image.');

  return $fields;
}
